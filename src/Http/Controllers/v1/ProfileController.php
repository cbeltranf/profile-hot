<?php
namespace Inmovsoftware\ProfileApi\Http\Controllers\V1;

use Inmovsoftware\ProfileApi\Http\Resources\V1\GlobalCollection;
use Inmovsoftware\ProfileApi\Models\V1\Profile;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;

class ProfileController extends Controller
{
    public function index(Request $request)
    {
        $filter = "name";
        $filterValue = $request->input("filterValue");
        $pageSize = $request->input("pageSize");
        $sortOrder = ($request->input("sortOrder") == "asc") ? "asc" : "desc";
        $sortField = "name";

        $item = Profile::orderBy($sortField, $sortOrder);
        if (!empty($filterValue)) {
            $item->where($filter, 'like', "%$filterValue%");
        }

        if (empty($pageSize)) {
            $pageSize = 10;
        }
        $item->where('status', 'A');

        $Auth_user = auth('api')->user();
        $item->where("it_business_id","=", $Auth_user->it_business_id);

        return new GlobalCollection($item->paginate($pageSize));
    }

    public function index_select(Request $request)
    {
        $item = Profile::orderBy("name", "asc");
        $item->where('status', 'A');
        $Auth_user = auth('api')->user();
        $item->where("it_business_id","=", $Auth_user->it_business_id);
        return response()->json($item->get());
    }



    public function store(Request $request)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C",
            "level" => "required|in:1,0"
        ]);
        $InsertId = Profile::insertGetId($data);
        $inserted = Profile::where("id", $InsertId)->get();
        return response()->json($inserted);
    }


    /**
     * Display the specified resource.
     *
     * @param  Inmovsoftware\ProfileApi\Models\V1\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {

        return response()->json($profile);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Inmovsoftware\ProfileApi\Models\V1\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $data = $request->validate([
            "it_business_id" => "required|integer|exists:it_business,id",
            "name" => "required|max:50",
            "status" => "nullable|in:A,C",
            "level" => "required|in:1,0"
        ]);

        $profile->update($data);
        return response()->json($profile);



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Inmovsoftware\ProfileApi\Models\V1\Profile $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        $item = $profile->delete();
        if ($item) {
            return response()->json(
                [
                    'errors' => [
                        'status' => 401,
                        'messages' => [trans('exceptions.element_not_found')]
                    ]
                ],
                401
            );
                    } else {
            return response()->json($item);
                    }
    }


}
